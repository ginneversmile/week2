package com.company.b1;

public class Stack {
    static int MAX = 1000;
    int top = 0;
    int a[] = new int[MAX];

    Stack() {
        top = -1;
    }

    boolean isEmpty() {
        return this.top < 0;
    }

    boolean push(int x) {
        if (top >= MAX - 1) {
            System.out.println("stack overflow");
            return false;
        } else {
            a[++top] = x;
            return true;
        }
    }

    int pop() {
        if (top < 0) {
            System.out.println("stack underflow");
            return 0;
        }
        return a[top--];
    }

    int peek() {
        if (top < 0) {
            System.out.println("stack underflow");
            return 0;
        }
        return a[top];
    }
}
