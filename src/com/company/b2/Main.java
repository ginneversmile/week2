package com.company.b2;

// Cho hai lớp Country và City có các thuộc tính (như bên dưới). Một quốc gia có 1 thủ đô và có thể có 1 hoặc nhiều thành phố. Một thành phố luôn trực thuộc chỉ 1 quốc gia.
//         - Country:
//         + String code (mã quốc gia)
//         + String name
//         + String continent (mã lục địa)
//         + double surfaceArea (diện tích bề mặt)
//         + int population (dân số)
//         + double gnp (Gross National Product)
//         + int capital (mã thành phố là thủ đô của đất nước này)
//         - City:
//         + int id
//         + String name
//         + int population (dân số của thành phố)


import java.util.*;

public class Main {
    public static void main(String[] args) {
        Map<Country, ArrayList<City>> store = new HashMap<>();
        initData(store);
        findMostPopulousCityInCountry(store);
        findMostPopulousCityInContinent(store);
        findMostPopulousCapital(store);
        findMostPopulousCapitalInContinent(store);
        sortCountryWithDescNumberOfCities(store);
        sortCountry(store);
    }


    public static Map<String, ArrayList<City>> getAllCitiesInContinent(Map<Country, ArrayList<City>> store) {
        Map<String, ArrayList<City>> temp = new HashMap<>();

        store.forEach((k, v) -> {
            ArrayList<City> m = temp.getOrDefault(k.getContinent(), new ArrayList<>());
            m.addAll(v);
            temp.put(k.getContinent(), m);
        });
        return temp;
    }

    public static Map<String, ArrayList<City>> getAllCapitalsInContinent(Map<Country, ArrayList<City>> store) {
        Map<String, ArrayList<City>> temp = new HashMap<>();

        store.forEach((k, v) -> {
            ArrayList<City> m = temp.getOrDefault(k.getContinent(), new ArrayList<>());
            //
            int cap = k.getCapital();
            Optional<City> op = v.stream().filter(i -> i.getId() == cap).findFirst();
            op.ifPresent(m::add);
            //
            temp.put(k.getContinent(), m);
        });
        return temp;
    }

    public static void sortCountry(Map<Country, ArrayList<City>> store) {
        System.out.println("-------- sortCountry------------");
        ArrayList<Country> m = new ArrayList<>();
        store.forEach((k, v) -> m.add(k));
        m.stream().sorted((a, b) -> b.getPopulation() - a.getPopulation()).filter(i -> i.getPopulation() != 0).forEach(System.out::println);
        System.out.println("--------------------");
    }

    public static void sortCountryWithDescNumberOfCities(Map<Country, ArrayList<City>> store) {
        System.out.println("-------- sortCountryWithDescNumberOfCities------------");
        ArrayList<Country> m = new ArrayList<>();
        store.forEach((k, v) -> m.add(k));
        m.stream().sorted((a, b) -> store.get(b).size() - store.get(a).size()).forEach(System.out::println);
        System.out.println("--------------------");
    }

    public static void findMostPopulousCapitalInContinent(Map<Country, ArrayList<City>> store) {

        System.out.println("-------- findMostPopulousCapitalInContinent------------");
        Map<String, ArrayList<City>> m = getAllCapitalsInContinent(store);
        m.forEach((k, v) -> {
            Optional<City> op = v.stream().max(Comparator.comparingInt(City::getPopulation));
            op.ifPresent(city -> {
                System.out.print(k);
                System.out.print(" ---> ");
                System.out.println(city);
            });
        });
        System.out.println("--------------------");
    }

    public static void findMostPopulousCapital(Map<Country, ArrayList<City>> store) {
        System.out.println("--------findMostPopulousCaptital------------");
        ArrayList<City> m = new ArrayList<>();
        store.forEach((k, v) -> {
            int cap = k.getCapital();
            Optional<City> op = v.stream().filter(i -> i.getId() == cap).findFirst();
            op.ifPresent(m::add);
        });
        Optional<City> result = m.stream().max(Comparator.comparingInt(City::getPopulation));
        if (result.isPresent()) {
            System.out.println(result.get());
        } else {
            System.out.println("NOT FOUND");
        }

        System.out.println("--------------------");
    }

    public static void findMostPopulousCityInCountry(Map<Country, ArrayList<City>> store) {
        System.out.println("----------findMostPopulousCityInCountry----------");
        Map<Country, City> temp = new HashMap<>();
        store.forEach((k, v) -> {
            Optional<City> optional = v.stream().max(
                    Comparator.comparingInt(City::getPopulation)
            );
//            optional.ifPresent(System.out::println);
            optional.ifPresent(city -> temp.put(k, city));
        });
        temp.forEach((k, v) -> {
            System.out.print(k);
            System.out.print(" ----> ");
            System.out.println(v);
        });

        System.out.println("--------------------");
    }

    public static void findMostPopulousCityInContinent(Map<Country, ArrayList<City>> store) {

        System.out.println("-----------findMostPopulousCityInContinent---------");
        Map<String, ArrayList<City>> st = getAllCitiesInContinent(store);
        Map<String, City> result = new HashMap<>();
        st.forEach((k, v) -> {
            Optional<City> op = v.stream().max(Comparator.comparingInt(City::getPopulation));
            op.ifPresent(city -> result.put(k, city));
        });

        result.forEach((k, v) -> {
            System.out.print(k);
            System.out.print(" ----> ");
            System.out.println(v);
        });

        System.out.println("--------------------");
    }

    public static void initData(Map<Country, ArrayList<City>> store) {
        //
        Country country = new Country("1A", "a", "North", 200, 70, 2000.0, 1);
        ArrayList<City> arr = new ArrayList<>();
        arr.add(new City(1, "1A-bc", 10));
        arr.add(new City(2, "1A-bcd", 20));
        arr.add(new City(10, "1A-bcdf", 40));
        store.put(country, arr);
        //
        Country country2 = new Country("1B", "b", "South", 300, 40, 1000.0, 4);
        ArrayList<City> arr2 = new ArrayList<>();
        arr2.add(new City(3, "1B-abc", 10));
        arr2.add(new City(4, "1B-abcd", 30));
        store.put(country2, arr2);
        //
    }
}